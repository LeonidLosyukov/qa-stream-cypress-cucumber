import { Before } from '@badeball/cypress-cucumber-preprocessor';
import MainPage from '../../pageobjects/main.page'

Before({ tags: '@go-to-main-page' }, function () {
  MainPage.navigateToMainPage();
});