import { Then, DataTable } from '@badeball/cypress-cucumber-preprocessor';
import MainPage from '../../pageobjects/main.page'
import NavigationMenu from '../../pageobjects/components/navigationMenu.component';

Then('I should see navigation menu', async() => {
  NavigationMenu.getNavigationMenu().should('be.visible');
});

Then('I should see logo in navigation menu', async() => {
  NavigationMenu.getNavigationMenuLogo().should('be.visible');
});

Then('I should see all these navigation menu links:', async(table: DataTable) => {
  table.raw().forEach(navMenuLink => NavigationMenu.getNavigationMenuItemByName(navMenuLink.toString()).should('be.visible'));
});

Then('I should see at least {int} sections', async(sectionsNumber: string) => {
  MainPage.getSections().should('have.length.above', parseInt(sectionsNumber, 10));
});

Then('I should see {string} section', async(sectionName: string) => {
  MainPage.getSection(sectionName).should('be.visible');
});

Then('I should see {string} and {string} section', async(firstSectionName: string, secondSectionName: string) => {
  MainPage.getSection(firstSectionName).should('be.visible');
  MainPage.getSection(secondSectionName).should('be.visible');
});