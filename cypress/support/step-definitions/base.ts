import { Given } from '@badeball/cypress-cucumber-preprocessor';
import MainPage from '../../pageobjects/main.page'

Given('I go to Geniusee main page', () => {
  MainPage.navigateToMainPage();
});