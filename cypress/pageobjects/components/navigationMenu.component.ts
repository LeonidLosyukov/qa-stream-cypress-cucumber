import Page from '../page';

class NavigationMenu extends Page {

  navMenuLinks = {
    'Estimator': 'https://estimate.geniusee.com/',
    'Services': '#',
    'Industries': '#',
    'Portfolio': 'https://geniusee.com/portfolio',
    'Blog': 'https://geniusee.com/blog',
    'Career': 'https://geniusee.com/career',
    'About': '#'
  }

  getNavigationMenu = () => cy.get('ul.nav-ul');

  getNavigationMenuLogo = () => cy.get('div.logo-box');

  getNavigationMenuItemByName = (navLinkName: string) =>
    cy.get('ul[class=nav-ul]').find('a')
      .contains(navLinkName)
      .should('have.attr', 'href', this.navMenuLinks[navLinkName]);

}

export default new NavigationMenu();