import Page from './page';

class MainPage extends Page {

  sectionsLocators = {
    'Our Goal': 'section#ourgoal',
    'Cases': 'section#cases',
    'Our teammates certifications': 'section[class=mt0]',
    'Feedback': 'section#feedback',
    'Benefits': 'section#benefits',
    'Geniusee in numbers': 'section#numbers',
    'Partners': 'section#partners',
    'Blog': 'section#blog',
    'Contact': 'section#contact'
  }

  getSections = () => cy.get('section');

  getSection = (sectionName: string) => cy.get(this.sectionsLocators[sectionName]);

}

export default new MainPage();