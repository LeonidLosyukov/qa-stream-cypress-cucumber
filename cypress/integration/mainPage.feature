Feature: Verify Main Page is visible

  Here we can add some additional description. E.g. some requires preconditions description or
  some details related to all the scenarios.

#  Background:
#    Given I go to Geniusee main page

  @go-to-main-page
  Scenario: The navigation menu on main page is visible
#    Given I go to Geniusee main page
    Then I should see navigation menu
    And I should see logo in navigation menu
    And I should see all these navigation menu links:
      | Estimator  |
      | Services   |
      | Industries |
      | Portfolio  |
      | Blog       |
      | Career     |
      | About      |

  @go-to-main-page
  Scenario: The Feedback section on main page is visible
#    Given I go to Geniusee main page
    Then I should see at least 7 sections
    And I should see "Feedback" section

  @go-to-main-page
  Scenario: The Benefits and Partners sections on main page are visible
#    Given I go to Geniusee main page
    Then I should see at least 7 sections
#    And I should see "Benefits" section
#    And I should see "Partners" section
    And I should see "Benefits" and "Partners" section